package com.example.ppmproject;

import java.io.Serializable;
import java.io.SerializablePermission;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UserProfile implements Serializable {

    private ArrayList<ProfileLine> profileLines;
    private String name ;
    private String motto;

    public UserProfile()
    {
        this.name = "";
        profileLines =  new ArrayList<>();
    }

    public UserProfile(String data){
        profileLines =  new ArrayList<>();
        String str[] = data.split("\n");
        for(int i = 0; i < str.length; i++){
            String datas[] = str[i].split(",");
            ProfileLine profileLine = new ProfileLine (datas[0], datas[1], datas[2], Integer.parseInt(datas[3]), Integer.parseInt(datas[4]));
            profileLines.add(profileLine);
        }
    }


    public void addUserProfile(String name, String date, String levelName, int numberParked, int time){
        profileLines.add(new ProfileLine( name,  date,  levelName,  numberParked,  time));
    }

    public  void setName(String name){
        this.name = name;
    }

    public  String getName(){
        return name;
    }


    public  void setMotto(String motto){
        this.motto = motto;
    }

    public  String getMotto(){
        return motto;
    }


    public ArrayList<ProfileLine> getProfileLines() {
        return profileLines;
    }

    public String toString(){
        String str= "";
        for(ProfileLine p : profileLines){
            str+= (p.toString()+"\n");
        }
        return str;
    }
}
