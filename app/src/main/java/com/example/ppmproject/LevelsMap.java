package com.example.ppmproject;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ppmproject.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Select the level on the world map
 * Google map + http request
 */
public class LevelsMap extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private DownloadWebpageTask downloadTask = new DownloadWebpageTask();
    private Marker currentMarker;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.levels_map);
         userProfile = (UserProfile)getIntent().getExtras().getSerializable("Profile");
         Button b = findViewById(R.id.radioButton);
         b.setTextColor(Color.BLACK);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Network available ...
            downloadTask.execute("https://www.lrde.epita.fr/~renault/teaching/ppm/2019/levels.txt");
            System.out.println("HELLO");

        } else {
            // Network not available ...
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("Title", "No network");
            intent.putExtra("Profile", userProfile);

            startActivity(intent);

        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        mMap.setOnMarkerClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(0,0)));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.getTag();
        Button b = findViewById(R.id.radioButton);
        b.setEnabled(true);
        marker.getTitle();
        currentMarker = marker;
        return false;
    }

    public void startGame(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Title", currentMarker.getTitle());
        intent.putExtra("Profile", userProfile);

        startActivity(intent);
    }


    private class DownloadWebpageTask
            extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call:
            // params[0] is the url.
            try {
                System.out.println("TEST ");

                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve URL (maybe invalid).";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            String[] str = result.split("\n");
            StringBuffer buf = new StringBuffer();
            int j = 0;
            for (int i = 1; i < str.length; i++) {
                 Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(str[i].split(",")[2]), Double.parseDouble(str[i].split(",")[1])))
                         .title(str[i].split(",")[0]));
                 m.setTag(i);

            }
        }

        private String downloadUrl(String myurl) throws IOException {
            InputStream is = null;
            // Only display the first 500 characters of the retrieved web page content.
            int len = 500;
            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                System.out.println("TEST ");

                int response = conn.getResponseCode();
                is = conn.getInputStream();
                // Convert the InputStream into a string
                String contentAsString = readIt(is, len);

                return contentAsString;
                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            }
            catch (IOException e){
                e.printStackTrace();
            }
            finally {
                if (is != null)
                    is.close();
            }
            return "";
        }

        private String readIt(InputStream is, int len){
            StringBuffer buf = new StringBuffer();

            try {
                int c;
                while ((c = is.read()) != -1){
                    buf.append((char)c);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return buf.toString();

        }
    }
}
