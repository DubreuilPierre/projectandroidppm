package com.example.ppmproject;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Path;
import android.media.Image;
import android.net.sip.SipSession;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Animation of the ships
 */
public class RotateUpdateListener implements ValueAnimator.AnimatorUpdateListener {
    private float oldPositionX = 0.0f;
    private float oldPositionY = 0.0f;
    private View image;
    private double degree = 0.0;
    private int tag;
    private float x = 0.0f;
    private float y = 0.0f;
    private Path animationPath;
    private Path parkAnimationPath;
    private Path parkAnimationPath2;
    private Path parkAnimationPath3;
    private Path parkAnimationPath4;
    private Path parkAnimationPath5;
    private Path parkAnimationPath6;

    private Path path = new Path();
    private ObjectAnimator anim;
    private boolean parkAnimation = false;
    private boolean isParking = false;
    private DrawView drawView;
    private boolean endAnimation = true;
    private float xAnim;
    private float yAnim;
    private float toX;
    private float toY;
    private static int cpt = 0;
    private List<Path> toSupress;
    Timer timer = new Timer();
    private ObjectAnimator anim2;
    private ObjectAnimator anim3;
    private ObjectAnimator anim4;
    private boolean noAnimationListener;
    private int cptAnimation = 0;
    private float xEnd = 0.0f;
    private float yEnd = 0.0f;
    private ImageView imageShip;

    /**
     * Animation and rotation of the ship
     * Cette classe permet les animations et la rotation des vaisseaux.
     * @param image
     * @param tag
     * @param toSupress
     * @param path
     * @param noAnimationListener
     */
    public RotateUpdateListener(View image, int tag, ArrayList<Path> toSupress, Path path, boolean noAnimationListener, ImageView imageShip){

        this.image = image;
        this.tag = tag;
        this.toSupress = new ArrayList<Path>();
        this.toSupress = toSupress;
        this.path = path;
        this.drawView = drawView;
        parkAnimationPath = new Path();
        parkAnimationPath2 = new Path();
        parkAnimationPath3 = new Path();
        parkAnimationPath4 = new Path();
        parkAnimationPath5 = new Path();
        parkAnimationPath6 = new Path();
        this.imageShip = imageShip;

        this.noAnimationListener = noAnimationListener;

    }

    /**
     * The rotation is recalculated at every AnimationUpdate. So we can now how to turn the ship depending of the traject the user dragged.
     * @param animation
     */
    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        if (parkAnimation) {
            if (animation != null)
                animation.cancel();
            parkAnimation = false;
            if (parkAnimationPath != null) {

                anim = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath);
                anim.addListener(new ShrinkAnimatorListener(0.5f));
                anim.setDuration(1000);
                anim.setInterpolator(new LinearInterpolator());
                anim.addUpdateListener(this);

                anim2 = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath2);
                anim2.addListener(new ShrinkAnimatorListener(0.25f));
                anim2.setDuration(1000);
                anim2.setStartDelay(2000);
                anim2.setInterpolator(new LinearInterpolator());
                anim2.addUpdateListener(this);

                anim3 = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath3);
                anim3.addListener(new ReverseAnimatorListener());
                anim3.setDuration(1000);
                anim3.setStartDelay(3000);
                anim3.setInterpolator(new LinearInterpolator());
                anim3.addUpdateListener(this);


                anim.start();
                anim2.start();
                anim3.start();
            }
        } else {
            x = (float) animation.getAnimatedValue("x");
            y = (float) animation.getAnimatedValue("y");
            if (!(oldPositionX == 0 && oldPositionY == 0)) {

                double DX = (x - oldPositionX);
                double D = Math.pow(DX, 2.0) + Math.pow((double) (y - oldPositionY), 2.0);


                D = Math.sqrt(D);

                if (D != 0.0) {
                    double D2 = x - oldPositionX;
                    double acos = Math.acos(D2 / D);
                    degree = Math.toDegrees(acos);
                    if (oldPositionY < y) {
                        degree = -degree;
                    }
                }

                double xprim = (image.getWidth() / 2.0) * Math.cos(Math.toRadians(degree)) + (0) * Math.sin(Math.toRadians(degree));
                double yprim = (-image.getWidth() / 2.0) * Math.sin(Math.toRadians(degree)) + (0) * Math.cos(Math.toRadians(degree));
                xprim = xprim + x + image.getWidth() / 2.0f;
                yprim = yprim + y + image.getHeight() / 2.0f;
                Path p = new Path();
                p.moveTo(oldPositionX, oldPositionY);
                p.lineTo((float) xprim, (float) yprim);
                image.setRotation(-(float) degree);
                if (!toSupress.isEmpty() && cpt % 2 == 0) {
                    System.out.println("CPT = "+cpt);
                    if (drawView != null) {
                        drawView.setPath(toSupress.get(0), tag);
                        switch(cptAnimation%4){
                            case 0: imageShip.setImageResource(R.drawable.red1);
                                break;
                            case 1: imageShip.setImageResource(R.drawable.red2);
                                break;
                            case 2: imageShip.setImageResource(R.drawable.red3);
                                break;
                            case 3: imageShip.setImageResource(R.drawable.red4);
                                break;
                        }
                        cptAnimation++;


                        toSupress.remove(0);
                    }
                }
            }
            oldPositionX = x;
            oldPositionY = y;
        }
    }

    public float getDegree(){
        return -(float)degree;
    }

    public void setDrawView(DrawView drawView){
        this.drawView = drawView;
    }
    public void animationPark( float x, float y, float toX, float toY){

        parkAnimationPath.moveTo(x,y);
        xEnd = x;
        yEnd = y;
        if(x == toX){
            if(y < toY){
                parkAnimationPath.lineTo(x, y + (toY-y)/3.0f );

                parkAnimationPath2.moveTo(x, y + (toY-y)/3.0f );

                parkAnimationPath2.lineTo(x, y + (toY-y)/2.0f );
                parkAnimationPath3.moveTo(x, y + (toY-y)/2.0f );
                parkAnimationPath3.lineTo(x, toY );

                parkAnimationPath4.moveTo(x, toY);
                parkAnimationPath4.lineTo(x, y + (toY-y)/2.0f );
                parkAnimationPath5.moveTo(x, y + (toY-y)/2.0f );
                parkAnimationPath5.lineTo(x, y + (toY-y)/3.0f );
                parkAnimationPath6.moveTo(x, y + (toY-y)/3.0f );
                parkAnimationPath6.lineTo(x, y);

            }
            else{
                parkAnimationPath.lineTo(x, y - (y-toY)/3.0f );

                parkAnimationPath2.moveTo(x, y - (y-toY)/3.0f );
                parkAnimationPath2.lineTo(x, y - (y-toY)/2.0f );

                parkAnimationPath3.moveTo(x, y - (y-toY)/2.0f );
                parkAnimationPath3.lineTo(x, toY );


                parkAnimationPath4.moveTo(x, toY);
                parkAnimationPath4.lineTo(x, y - (y-toY)/2.0f );
                parkAnimationPath5.moveTo(x, y - (y-toY)/2.0f );
                parkAnimationPath5.lineTo(x, y - (y-toY)/3.0f );
                parkAnimationPath6.moveTo(x, y - (y-toY)/3.0f );
                parkAnimationPath6.lineTo(x, y);

            }
        }
        else{
            if(x < toX) {
                parkAnimationPath.lineTo(x + (toX - x) / 3.0f, y);

                parkAnimationPath2.moveTo(x + (toX - x) / 3.0f, y);

                parkAnimationPath2.lineTo(x + (toX - x) / 2.0f, y);
                parkAnimationPath3.moveTo(x + (toX - x) / 2.0f, y);
                parkAnimationPath3.lineTo(toX, y);

                parkAnimationPath4.moveTo(toX, y);
                parkAnimationPath4.lineTo(x + (toX - x) / 2.0f, y);
                parkAnimationPath5.moveTo(x + (toX - x) / 2.0f, y );
                parkAnimationPath5.lineTo(x + (toX - x) / 3.0f, y );
                parkAnimationPath6.moveTo(x + (toX - x) / 3.0f, y );
                parkAnimationPath6.lineTo(x, y);
            }
            else{
                parkAnimationPath.lineTo(x - (x - toX) / 3.0f, y);

                parkAnimationPath2.moveTo(x - (x - toX) / 3.0f, y);

                parkAnimationPath2.lineTo(x - (x - toX) / 2.0f, y);
                parkAnimationPath3.moveTo(x - (x - toX) / 2.0f, y);
                parkAnimationPath3.lineTo(toX, y);

                parkAnimationPath4.moveTo(toX, y);
                parkAnimationPath4.lineTo(x - (x - toX) / 2.0f, y);
                parkAnimationPath5.moveTo(x - (x - toX) / 2.0f, y );
                parkAnimationPath5.lineTo(x - (x - toX) / 3.0f, y );
                parkAnimationPath6.moveTo(x -(x - toX) / 3.0f, y );
                parkAnimationPath6.lineTo(x, y);

            }

        }
        parkAnimation = true;

        if(noAnimationListener) {
            onAnimationUpdate(new ValueAnimator());
        }

    }

    public void cancel() {

    }



    private class ShrinkAnimatorListener implements Animator.AnimatorListener {
        private float scale;

        public ShrinkAnimatorListener(float scale){
            this.scale = scale;
        }
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            image.setScaleX(scale);
            image.setScaleY(scale);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }

    private class ReverseAnimatorListener implements Animator.AnimatorListener {

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            anim = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath4);
            anim.addListener(new ShrinkAnimatorListener(0.25f));
            anim.setDuration(1000);
            anim.setStartDelay(1000);
            anim.setInterpolator(new LinearInterpolator());

            anim2 = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath5);
            anim2.addListener(new ShrinkAnimatorListener(0.5f));
            anim2.setDuration(1000);
            anim2.setStartDelay(2000);
            anim2.setInterpolator(new LinearInterpolator());

            anim3 = new ObjectAnimator().ofFloat(image, "x", "y", parkAnimationPath6);
            anim3.addListener(new ShrinkAnimatorListener(1.0f));
            anim3.setDuration(1000);
            anim3.setStartDelay(3000);
            anim3.setInterpolator(new LinearInterpolator());


            Path p = new Path();
            p.moveTo(xEnd, yEnd);
            p.lineTo(xEnd, 4000.0f);
            anim4 = new ObjectAnimator().ofFloat(image, "x", "y", p);
            anim4.setStartDelay(4000);
            anim4.setDuration(10000);
            anim4.setInterpolator(new LinearInterpolator());
            anim4.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    image.setRotation(90);

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            anim.start();
            anim2.start();
            anim3.start();
            anim4.start();


        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }

    public void end(){
        if(anim != null)
            anim.cancel();
        if(anim2 != null)
            anim2.cancel();
        if(anim3 != null)
            anim3.cancel();
        if(anim4 != null)
            anim4.cancel();
    }


}



