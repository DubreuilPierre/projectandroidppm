package com.example.ppmproject;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.Timer;

/**
 * Change your name, your profile pic, or your motto.
 * Displays your high score.
 *
 */
public class UserProfileActivity extends AppCompatActivity {

    private final int GALLERY_REQUEST_CODE = 1;
    UserProfile userProfile;
    ImageView pic;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        userProfile = (UserProfile)getIntent().getExtras().getSerializable("Profile");
        final EditText username =  findViewById(R.id.username);

        //Changing the name
        username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    userProfile.setName(v.getText().toString().replaceAll("\\r", ""));
                }
                return false;
            }
        });

        final EditText motto =  findViewById(R.id.motto);
        username.setText(userProfile.getName());
        motto.setImeOptions(EditorInfo.IME_ACTION_DONE);
        motto.setRawInputType(InputType.TYPE_CLASS_TEXT);
        motto.setMaxLines(3);

        //changing the motto
        if(userProfile.getMotto() != "")
            motto.setText(userProfile.getMotto());
            motto.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    userProfile.setMotto(v.getText().toString());
                }
                return false;
            }
        });



         pic = findViewById(R.id.pic);
        pic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, GALLERY_REQUEST_CODE);

                }
                return false;
            }


        });

        TextView scores = findViewById(R.id.score);
        scores.setText("MY SCORES :\n"+userProfile.toString());



        }


    /**
     * Si l'application est quitté on sauvegarde les informations du profil
     * Save when the activity is stopped
     */
    @Override
    protected void onStop() {
        try {
            SaveToFile.saveTo(SaveToFile.USERPROFILE, getApplicationContext(), userProfile);
            SaveToFile.saveTo(SaveToFile.USERNAME, getApplicationContext(), userProfile);
            SaveToFile.saveTo(SaveToFile.USERMOTTO, getApplicationContext(), userProfile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    /*
        Pick a profile pic from the gallery
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            if(requestCode == GALLERY_REQUEST_CODE){
                Uri selectedImage = data.getData();
                pic.setImageURI(selectedImage);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
}
