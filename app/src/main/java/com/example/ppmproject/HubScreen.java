package com.example.ppmproject;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import java.io.IOException;


public class HubScreen extends AppCompatActivity {

    /**
     * Central hug, you can choose between the game, your profile or others users high scores.
     * Classe du Hub Central, elle permet de choisir entre le jeu, le profile et les scores des autres joueurs.
     */
    UserProfile userProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_selection);

        /**
         * Fetch the data from the save files.
         * On recupere les données stockées dans les fichiers motto et score
         */
        userProfile = SaveToFile.readFrom(SaveToFile.USERPROFILE, getApplicationContext());
        String motto = SaveToFile.readFromMotto(SaveToFile.USERMOTTO, getApplicationContext());
        String name = SaveToFile.readFromUsername(SaveToFile.USERNAME, getApplicationContext());
        userProfile.setName(name.replaceAll("\\n", ""));
        userProfile.setMotto(motto);

        /**
         * Music service
         * Service pour la musique
         */

        doBindService();
        Intent music = new Intent();
        music.setClass(this, MusicService.class);
        startService(music);
        HomeWatcher mHomeWatcher;

        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                if (mServ != null) {
                    mServ.pauseMusic();
                }
            }
            @Override
            public void onHomeLongPressed() {
                if (mServ != null) {
                    mServ.pauseMusic();
                }
            }
        });
        mHomeWatcher.startWatch();

    }


    /**
     * Share the user profile with the other activities.
     * On transmet dans l'intent le profile du joueur, on s'en servira pour les lignes de scores
     * @param view
     */

    public void startGame(View view){

        Intent intent = new Intent(this, LevelsMap.class);
        intent.putExtra("Profile", userProfile);

        startActivity(intent);
    }

    public void startUsers(View view){

        Intent intent = new Intent(this, UsersScreen.class);
        startActivity(intent);
    }

    public void profile(View view){
        userProfile = SaveToFile.readFrom(SaveToFile.USERPROFILE, getApplicationContext());
        String motto = SaveToFile.readFromMotto(SaveToFile.USERMOTTO, getApplicationContext());
        String name = SaveToFile.readFromUsername(SaveToFile.USERNAME, getApplicationContext());
        userProfile.setName(name.replaceAll("\\n", ""));
        userProfile.setMotto(motto);
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra("Profile", userProfile);
        System.out.println("PROFILE");
        startActivity(intent);
    }


    /**
     * If the activity is Destoyed, save the user informations.
     * Stop the music.
     * Si l'application est quitté on sauvegarde les informations du profil
     */

    @Override
    protected void onDestroy() {

        try {
            SaveToFile.saveTo(SaveToFile.USERPROFILE, getApplicationContext(), userProfile);
            SaveToFile.saveTo(SaveToFile.USERMOTTO, getApplicationContext(), userProfile);

        } catch (IOException e) {
            e.printStackTrace();
        }


        doUnbindService();
        Intent music = new Intent();
        music.setClass(this,MusicService.class);
        stopService(music);
        super.onDestroy();


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mServ != null) {
            mServ.resumeMusic();
        }

    }

    private boolean mIsBound = false;
    private MusicService mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((MusicService.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,MusicService.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        PowerManager pm = (PowerManager)
                getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = false;
        if (pm != null) {
            isScreenOn = pm.isScreenOn();
        }

        if (!isScreenOn) {
            if (mServ != null) {
                mServ.pauseMusic();
            }
        }

    }
}


