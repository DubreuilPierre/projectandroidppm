package com.example.ppmproject;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SyncStats;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * Handling the movement of the ships by the user
 * Mostly calculation of coordinates, rotations and collisions.
 */

public class ShipFragment extends Fragment implements View.OnTouchListener {

    private Path path = new Path();
    private Path animationPath = new Path();
    private ArrayList<Path> toSupress= new ArrayList<>();
    private ImageView image;
    private View view = null;

    private DrawView drawView = null;
    private ObjectAnimator anim = null;
    private RotateUpdateListener rotateListener;

    private Vector<Float> a;
    private Vector<Float> b;
    private Vector<Float> c;
    private Vector<Float> d;
    private float oldPositionX = 0.0f;
    private float oldPositionY = 0.0f;
    private int cptMove = 0;
    private Vector<Float> ab = new Vector<>();
    private Vector<Float> am = new Vector<>();
    private Vector<Float> bc = new Vector<>();
    private Vector<Float> bm = new Vector<>();
    private boolean end;
    private Vector<Float> start = new Vector<>();
    private boolean down = false;
    private boolean firstTime = true;
    private Timer timer = new Timer();
    private boolean endAnimation = true;
    private boolean isEntry = false;
    private PointFloat explosionPoint;
    public boolean isCharged() {
        return isCharged;
    }
    private boolean isCharged = false;
    private boolean isOnMap = false;
    private Object mutex = new Object();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ship, container, false);
        view.setOnTouchListener(this);

        image = view.findViewById(R.id.imageView);
        drawView = view.getRootView().findViewById(R.id.drawView);
        rotateListener = new RotateUpdateListener(view,Integer.parseInt(getTag()), toSupress, path, true, image);


        return view;
    }




    public PointFloat getExplosionPoint(){
        return explosionPoint;
    }

    public void setup(float x, float y, boolean reset){
        System.out.println("Setup");
        view.setX(x);
        view.setY(y);
        isCharged = true;
        isOnMap = true;
        isEntry = true;
        view.setRotation(0.0f);
        image.setImageResource(R.drawable.red1);
        if(reset) {
            rotateListener.end();
        }
        rotateListener = new RotateUpdateListener(view,Integer.parseInt(getTag()), toSupress, path, true, image);


    }

    public boolean getIsEntry(){
        return isEntry;
    }


    /**
     * D'abord, on regarde si le vaisseau est en animation de marchandise. Si il ne l'est pas on regarde si l'appui est réelement dans le rectangle de nos vaisseau.
     * Cela sert a distinguer le cas ou l'on drag hors du vaisseau ou si l'on reste à l'interieur.
     * @param v
     * @param event
     * @return
     */
    public  boolean onTouch(View v, MotionEvent event) {
        synchronized(mutex) {
            isEntry = false;
            if (endAnimation) {
                if (isCharged) {
                    boolean isinside = false;
                    double degree = 0.0;
                    drawView = v.getRootView().findViewById(R.id.drawView);
                    float eventX = event.getX();
                    float eventY = event.getY();

                    if (down) {
                        am.add(0, event.getX() + view.getX() - a.get(0));
                        am.add(1, event.getY() + view.getY() - a.get(1));

                        bm.add(0, event.getX() + view.getX() - b.get(0));
                        bm.add(1, event.getY() + view.getY() - b.get(1));

                        if (isInsideRect(ab, bc, bm, am)) {
                            isinside = true;
                        }
                    }

                    /**
                     * On calcule si le touch se trouve dans le rectangle
                     */
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            down = true;
                            firstTime = true;

                            a = getRectCoordinates("TL");
                            b = getRectCoordinates("TR");
                            c = getRectCoordinates("BR");
                            d = getRectCoordinates("BL");

                            ab.add(0, b.get(0) - a.get(0));
                            ab.add(1, b.get(1) - a.get(1));
                            bc.add(0, c.get(0) - b.get(0));
                            bc.add(1, c.get(1) - b.get(1));
                            view.setRotation(0f);
                            path.reset();
                            animationPath.reset();

                            start = startPath(path);

                            image.setRotation(rotateListener.getDegree());

                            if (anim != null) {
                                anim.cancel();
                            }

                            drawView.setPath(path, Integer.parseInt(getTag()));
                            break;

                        /**
                         * Si l'on a drag hors du rectangle alors on lance l'animation du vaisseau
                         */
                        case MotionEvent.ACTION_UP:
                            down = false;
                            image.setRotation(0f);
                            if (!isinside) {
                                rotateListener = new RotateUpdateListener(view,Integer.parseInt(getTag()), toSupress, path, false, image);
                                rotateListener.setDrawView(drawView);
                                anim = new ObjectAnimator().ofFloat(v, "x", "y", animationPath);
                                int vitesse = 0;

                                if(cptMove != 0) {
                                    vitesse = 50000 / cptMove;
                                }
                                if(vitesse < 2000){
                                    vitesse = 2000;
                                }
                                anim.setDuration(vitesse);

                                anim.setInterpolator(new LinearInterpolator());

                                anim.addUpdateListener(rotateListener);
                                anim.addListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        if (end) {
                                            path.reset();

                                            if (getTag() != null) {
                                                drawView.setPath(path, Integer.parseInt(getTag()));
                                            }
                                            toSupress.clear();
                                        }
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                        //path.reset();
                                        //drawView.setPath(path, Integer.parseInt(getTag()));

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                                end = true;
                                anim.start();
                            }
                            break;


                        /**
                         * On a deux path qu'il faut calculer, celui qui indique le chemin que prend le vaisseau
                         * et celui de l'animation. Pour le path il faut toujours qu'il soit au devant du vaisseau.
                         * Pour l'animation il faut toujours qu'elle soit coordonnée et suive le path qu'on dessine.
                         * Les tests qui sont réalisés sont ici pour créer directement un chemin entre le devant du vaisseau et là on se trouve
                         * le premier toucher à l'exterieur. Ainsi on va creer un chemin entre le devant et les cotes/derriere.
                         */
                        case MotionEvent.ACTION_MOVE:

                            if (!(oldPositionX == 0 && oldPositionY == 0)) {

                                double DX = (eventX + view.getX() - oldPositionX);
                                double D = Math.pow(DX, 2.0) + Math.pow((double) (eventY + view.getY() - oldPositionY), 2.0);

                                D = Math.sqrt(D);

                                if (D != 0.0) {
                                    double D2 = eventX + view.getX() - oldPositionX;

                                    double acos = Math.acos(D2 / D);
                                    degree = Math.toDegrees(acos);
                                    if (oldPositionY < eventY + view.getY()) {
                                        degree = -degree;
                                    }
                                }
                            }
                            if (!isinside) {
                                if (firstTime) {
                                    boolean boolup = false;
                                    boolean booldown = false;
                                    boolean boolright = false;
                                    boolean boolleft = false;
                                    double firstDegree = -rotateListener.getDegree();
                                    double firstDegreeAnimation = -rotateListener.getDegree();

                                    if (firstDegree == 0.0) {
                                        if (eventX > view.getWidth()) {
                                            boolright = true;
                                        }
                                        if (eventX < 0) {
                                            boolleft = true;
                                        }
                                        if (eventY > view.getHeight()) {
                                            booldown = true;
                                        }
                                        if (eventY < 0) {
                                            boolup = true;
                                        }
                                    } else if (firstDegree == -180.0) {
                                        if (eventX > view.getWidth()) {
                                            boolleft = true;
                                        }
                                        if (eventX < 0) {
                                            boolright = true;
                                        }
                                        if (eventY > view.getHeight()) {
                                            boolup = true;
                                        }
                                        if (eventY < 0) {
                                            booldown = true;
                                        }
                                    } else if (firstDegree > 85.0 && firstDegree < 95.0) {

                                        if (eventX > view.getWidth()) {
                                            booldown = true;
                                        }
                                        if (eventX < 0) {
                                            boolup = true;
                                        }
                                        if (eventY > view.getHeight()) {
                                            boolleft = true;
                                        }
                                        if (eventY < 0) {
                                            boolright = true;
                                        }
                                    } else if (firstDegree < -85.0 && firstDegree < -95.0) {
                                        if (eventX > view.getWidth()) {
                                            boolup = true;
                                        }
                                        if (eventX < 0) {
                                            booldown = true;
                                        }
                                        if (eventY > view.getHeight()) {
                                            boolright = true;
                                        }
                                        if (eventY < 0) {
                                            boolleft = true;
                                        }
                                    }

                                    if (where(b, c, eventX + view.getX(), eventY + view.getY()) || boolright) {

                                        path.lineTo(eventX + view.getX() + 20.0f, eventY + view.getY());
                                        setAnimationPath(eventX + view.getX() + 20.0f, eventY + view.getY(), firstDegree);

                                    } else if (where(a, b, eventX + view.getX(), eventY + view.getY()) || boolup) {
                                        double xprim = (((view.getWidth()) / 2.0f) - 20.0f) * Math.cos(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f)) * Math.sin(Math.toRadians(firstDegree));
                                        double yprim = -(((view.getWidth()) / 2.0f) - 20.0f) * Math.sin(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f)) * Math.cos(Math.toRadians(firstDegree));
                                        xprim = xprim + view.getX() + view.getWidth() / 2.0;
                                        yprim = yprim + view.getY() + view.getHeight() / 2.0;

                                        path.lineTo((float) xprim, (float) yprim);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }

                                        setAnimationPath((float) xprim, (float) yprim, firstDegreeAnimation);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }

                                        path.lineTo(eventX + view.getX(), eventY + view.getY() - 10.0f);
                                        setAnimationPath(eventX + view.getX(), eventY + view.getY() - 10.0f, firstDegreeAnimation);
                                        System.out.println("UP");


                                    } else if (where(c, d, eventX + view.getX(), eventY + view.getY()) || booldown) {
                                        double xprim = (((view.getWidth()) / 2.0f) - 20.0f) * Math.cos(Math.toRadians(firstDegree)) + (((view.getHeight()) / 2.0f) + 30.0f) * Math.sin(Math.toRadians(firstDegree));
                                        double yprim = -(((view.getWidth()) / 2.0f) - 20.0f) * Math.sin(Math.toRadians(firstDegree)) + (((view.getHeight()) / 2.0f) + 30.0f) * Math.cos(Math.toRadians(firstDegree));
                                        xprim = xprim + view.getX() + view.getWidth() / 2.0;
                                        yprim = yprim + view.getY() + view.getHeight() / 2.0;

                                        path.lineTo((float) xprim, (float) yprim);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }
                                        setAnimationPath((float) xprim, (float) yprim, firstDegreeAnimation);


                                        path.lineTo(eventX + view.getX(), eventY + view.getY() + 10.0f);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }
                                        setAnimationPath(eventX + view.getX(), eventY + view.getY() + 10.0f, firstDegreeAnimation);

                                        System.out.println("DOWN");
                                    } else if (where(a, d, eventX + view.getX(), eventY + view.getY()) || boolleft) {
                                        System.out.println("LEFT");

                                        double xprim = (((view.getWidth()) / 2.0f) - 20.0f) * Math.cos(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f) - 30.0f) * Math.sin(Math.toRadians(firstDegree));
                                        double yprim = -(((view.getWidth()) / 2.0f) - 20.0f) * Math.sin(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f) - 30.0f) * Math.cos(Math.toRadians(firstDegree));
                                        xprim = xprim + view.getX() + view.getWidth() / 2.0;
                                        yprim = yprim + view.getY() + view.getHeight() / 2.0;

                                        path.lineTo((float) xprim, (float) yprim);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }
                                        setAnimationPath((float) xprim, (float) yprim, firstDegreeAnimation);

                                        xprim = -(((view.getWidth()) / 2.0f) + 20.0f) * Math.cos(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f) - 30.0f) * Math.sin(Math.toRadians(firstDegree));
                                        yprim = (((view.getWidth()) / 2.0f) + 20.0f) * Math.sin(Math.toRadians(firstDegree)) + (-((view.getHeight()) / 2.0f) - 30.0f) * Math.cos(Math.toRadians(firstDegree));
                                        xprim = xprim + view.getX() + view.getWidth() / 2.0;
                                        yprim = yprim + view.getY() + view.getHeight() / 2.0;

                                        path.lineTo((float) xprim, (float) yprim);

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }
                                        setAnimationPath((float) xprim, (float) yprim, firstDegreeAnimation);

                                        path.lineTo(eventX + view.getX() - 10.0f, eventY + view.getY());

                                        firstDegreeAnimation += 90;
                                        if (firstDegreeAnimation > 180) {
                                            firstDegreeAnimation = 180 - firstDegreeAnimation;
                                        }
                                        setAnimationPath(eventX + view.getX() - 10.0f, eventY + view.getY(), firstDegreeAnimation);

                                    }
                                } else {

                                    if (cptMove % 7 == 0) {

                                        float x = view.getX() + eventX;
                                        float y = view.getY() + eventY;

                                        if (cptMove == 0) {
                                            toSupress.add(new Path(path));
                                        } else {
                                            Path p = new Path();
                                            p.moveTo(oldPositionX, oldPositionY);
                                            toSupress.add(p);
                                        }

                                        for (Path pathAdd : toSupress) {
                                            pathAdd.lineTo(x, y);
                                        }
                                        path.lineTo(x, y);


                                        float x2 = (view.getWidth()) / 2.0f;
                                        float y2 = 0.0f;//-(view.getHeight() - 100.0f) / 2.0f;
                                        float xprim = (float) (x2 * Math.cos(Math.toRadians(degree)) + y2 * Math.sin(Math.toRadians(degree)));
                                        float yprim = (float) (-x2 * Math.sin(Math.toRadians(degree)) + y2 * Math.cos(Math.toRadians(degree)));
                                        animationPath.lineTo(x - xprim - (view.getWidth()) / 2.0f, y - yprim - (view.getHeight()) / 2.0f);
                                    }
                                    cptMove++;
                                }
                                firstTime = false;
                            }
                            drawView.setPath(path, Integer.parseInt(getTag()));
                            oldPositionX = eventX + view.getX();
                            oldPositionY = eventY + view.getY();
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean where(Vector<Float> a, Vector<Float> b, float x, float y){

        if((x > a.get(0) && x < b.get(0)) || (x < a.get(0) && x > b.get(0))) {
            if ((y > a.get(1) && y < b.get(1)) || (y < a.get(1) && y > b.get(1))) {
                return true;
            }
        }
        return false;
    }
    private void setAnimationPath(float x, float y, double degree){
        float x2 = (view.getWidth() ) / 2.0f;
        float y2 = 0.0f;
        float xprim = (float) (x2 * Math.cos(Math.toRadians(degree)) + y2 * Math.sin(Math.toRadians(degree)));
        float yprim = (float) (-x2 * Math.sin(Math.toRadians(degree)) + y2 * Math.cos(Math.toRadians(degree)));
        animationPath.lineTo(x - xprim - (view.getWidth()) / 2.0f, y - yprim - (view.getHeight()) / 2.0f);

    }
    public Vector<Float> startPath(Path path) {

        double degree = -rotateListener.getDegree();

        double xprim = (view.getWidth() / 2.0) * Math.cos(Math.toRadians(degree)) + (0) * Math.sin(Math.toRadians(degree));
        double yprim = (-view.getWidth() / 2.0) * Math.sin(Math.toRadians(degree)) + (0) * Math.cos(Math.toRadians(degree));

        xprim = xprim + view.getX() + view.getWidth() / 2.0;
        yprim = yprim + view.getY() + view.getHeight() / 2.0;

        path.moveTo((float) xprim, (float) yprim);

        float x2 = (view.getWidth()) / 2.0f;
        float y2 = 0.0f;
        float xprim2 = (float) (x2 * Math.cos(Math.toRadians(degree)) + y2 * Math.sin(Math.toRadians(degree)));
        float yprim2 = (float) (-x2 * Math.sin(Math.toRadians(degree)) + y2 * Math.cos(Math.toRadians(degree)));
        animationPath.moveTo((float)xprim - xprim2 - (view.getWidth()) / 2.0f, (float)yprim - yprim2 - (view.getHeight()) / 2.0f);

        xprim = xprim + 20.0;
        path.lineTo((float) xprim, (float) yprim);
        setAnimationPath((float) xprim, (float) yprim, degree);

        Vector v = new Vector();
        v.add((float) xprim);
        v.add((float) yprim);
        return v;

    }

    public Vector<Float> getRectCoordinates(String corner) {
        double degree = -rotateListener.getDegree();
        double xprim;
        double yprim;
        switch (corner) {
            case "TL":
                xprim = (double) (-view.getWidth() / 2) * Math.cos(Math.toRadians(degree)) + (-view.getHeight() / 2.0) * Math.sin(Math.toRadians(degree));
                yprim = (double) (view.getWidth() / 2) * Math.sin(Math.toRadians(degree)) + (-view.getHeight() / 2.0) * Math.cos(Math.toRadians(degree));
                break;
            case "TR":
                xprim = (double) (view.getWidth() / 2) * Math.cos(Math.toRadians(degree)) + (-view.getHeight() / 2.0) * Math.sin(Math.toRadians(degree));
                yprim = (double) (-view.getWidth() / 2) * Math.sin(Math.toRadians(degree)) + (-view.getHeight() / 2.0) * Math.cos(Math.toRadians(degree));
                break;
            case "BR":
                xprim = (double) (view.getWidth() / 2) * Math.cos(Math.toRadians(degree)) + (view.getHeight() / 2.0) * Math.sin(Math.toRadians(degree));
                yprim = (double) (-view.getWidth() / 2) * Math.sin(Math.toRadians(degree)) + (view.getHeight() / 2.0) * Math.cos(Math.toRadians(degree));
                break;
            case "BL":
                xprim = (double) (-view.getWidth() / 2) * Math.cos(Math.toRadians(degree)) + (view.getHeight() / 2.0) * Math.sin(Math.toRadians(degree));
                yprim = (double) (view.getWidth() / 2) * Math.sin(Math.toRadians(degree)) + (view.getHeight() / 2.0) * Math.cos(Math.toRadians(degree));
                break;
            default:
                xprim = 0.0;
                yprim = 0.0;
                break;
        }

        xprim = xprim + view.getWidth() / 2.0 + view.getX();
        yprim = yprim + view.getHeight() / 2.0 + view.getY();
        Vector v = new Vector();
        v.add((float) xprim);
        v.add((float) yprim);
        return v;

    }

    public boolean isInsideRect(Vector<Float> ab, Vector<Float> bc, Vector<Float> bm, Vector<Float> am) {
        float ABAM = ab.get(0) * am.get(0) + ab.get(1) * am.get(1);


        if (ABAM < 0) return false;

        float ABAB = ab.get(0) * ab.get(0) + ab.get(1) * ab.get(1);

        if (ABAM > ABAB) return false;

        float BCBM = bc.get(0) * bm.get(0) + bc.get(1) * bm.get(1);

        if (BCBM < 0) return false;

        float BCBC = bc.get(0) * bc.get(0) + bc.get(1) * bc.get(1);

        if (BCBM > BCBC) return false;

        return true;
    }

    private Timer timer2 = new Timer();
    private int cptExplosion = 0;

    public boolean isCollision(ShipFragment f) {
        Vector<Vector> points = new Vector<>();
        points.add(0, f.getRectCoordinates("TL"));
        points.add(1, f.getRectCoordinates("TR"));
        points.add(2, f.getRectCoordinates("BR"));
        points.add(3, f.getRectCoordinates("BL"));

        Vector<Float> a2 = getRectCoordinates("TL");
        Vector<Float> b2 = getRectCoordinates("TR");
        Vector<Float> c2 = getRectCoordinates("BR");

        Vector<Float> ab2 = new Vector<>();
        Vector<Float> bc2 = new Vector<>();
        Vector<Float> am2 = new Vector<>();
        Vector<Float> bm2 = new Vector<>();

        ab2.add(0, b2.get(0) - a2.get(0));
        ab2.add(1, b2.get(1) - a2.get(1));
        bc2.add(0, c2.get(0) - b2.get(0));
        bc2.add(1, c2.get(1) - b2.get(1));

        for (Vector<Float> m0 : points) {
            am2.add(0, m0.get(0) - a2.get(0));
            am2.add(1, m0.get(1) - a2.get(1));

            bm2.add(0, m0.get(0) - b2.get(0));
            bm2.add(1, m0.get(1) - b2.get(1));

            if (isInsideRect(ab2, bc2, bm2, am2)) {

                explosionPoint = new PointFloat(m0.get(0), m0.get(1));
                return true;
            }
        }
        return false;
    }

    public  PointFloat isParked(PointFloat m0, PointFloat m1, PointFloat m2, PointFloat m3) {
        synchronized (mutex) {
            if (isEntry || !endAnimation || !isCharged) {
                return new PointFloat(-1.0f, -1.0f);
            }
            Vector<Float> ab2 = new Vector<>();
            Vector<Float> bc2 = new Vector<>();
            Vector<Float> am2 = new Vector<>();
            Vector<Float> bm2 = new Vector<>();

            Vector<Float> a2 = getRectCoordinates("TL");
            Vector<Float> b2 = getRectCoordinates("TR");
            Vector<Float> c2 = getRectCoordinates("BR");

            ab2.add(0, b2.get(0) - a2.get(0));
            ab2.add(1, b2.get(1) - a2.get(1));
            bc2.add(0, c2.get(0) - b2.get(0));
            bc2.add(1, c2.get(1) - b2.get(1));

            am2.add(0, m0.getX() - a2.get(0));
            am2.add(1, m0.getY() - a2.get(1));

            bm2.add(0, m0.getX() - b2.get(0));
            bm2.add(1, m0.getY() - b2.get(1));

            if (isInsideRect(ab2, bc2, bm2, am2))
                return m0;

            am2.add(0, m1.getX() - a2.get(0));
            am2.add(1, m1.getY() - a2.get(1));

            bm2.add(0, m1.getX() - b2.get(0));
            bm2.add(1, m1.getY() - b2.get(1));

            if (isInsideRect(ab2, bc2, bm2, am2))
                return m1;

            am2.add(0, m2.getX() - a2.get(0));
            am2.add(1, m2.getY() - a2.get(1));

            bm2.add(0, m2.getX() - b2.get(0));
            bm2.add(1, m2.getY() - b2.get(1));

            if (isInsideRect(ab2, bc2, bm2, am2))
                return m2;


            am2.add(0, m3.getX() - a2.get(0));
            am2.add(1, m3.getY() - a2.get(1));

            bm2.add(0, m3.getX() - b2.get(0));
            bm2.add(1, m3.getY() - b2.get(1));

            if (isInsideRect(ab2, bc2, bm2, am2))
                return m3;

            return new PointFloat(-1.0f, -1.0f);
        }
    }


    public synchronized void park(PointFloat m0, ImageView planet){

        isEntry = false;

        if(endAnimation && isCharged) {
            endAnimation = false;

            float x = 0.0f;
            float y = 0.0f;
            float toX = 0.0f;
            float toY = 0.0f;
            float degree = 0.0f;
            path.reset();
            animationPath.reset();

            if(m0.getX() == planet.getX() + planet.getWidth() /2.0f){
                if(m0.getY() == planet.getY()){
                    x = m0.getX() - view.getWidth()/2.0f;
                    y = m0.getY() - view.getHeight()/2.0f;
                    toX = x;
                    toY = y + planet.getHeight() / 2.0f;
                    degree = 90f;
                }
                else{
                    x = m0.getX() - view.getWidth()/2.0f;
                    y = m0.getY() - view.getHeight()/2.0f;
                    degree = 270f;
                    toX = x;
                    toY = y - planet.getHeight() /2.0f;
                }
            }
            else if(m0.getX() == planet.getX()){
                x = m0.getX() - view.getWidth()/2.0f;
                y = m0.getY() - view.getHeight()/2.0f;
                degree = 0f;
                toX = x + planet.getWidth() / 2.0f;
                toY = y;
            }
            else if(m0.getX() ==planet.getX() + planet.getWidth()){
                x = m0.getX() - view.getWidth()/2.0f;
                y = m0.getY() - view.getHeight()/2.0f;
                degree = 0f;
                toX = x - planet.getWidth() / 2.0f;
                toY = y;
            }


            Path parkAnimationPath = new Path();

            parkAnimationPath.moveTo(x, y);
            parkAnimationPath.lineTo(toX, toY);
            view.setRotation(degree);
            rotateListener.animationPark(x,y,toX,toY);
            timer.schedule(new GameObserver(), 8000);
        }
    }

    public void end(){
        rotateListener.cancel();
        // endAnimation = false;
    }

    public boolean getEndAnimation(){
        return endAnimation;
    }


    private  class GameObserver extends TimerTask {
        @Override
        public void run() {
            endAnimation = true;
            isCharged = false;
            image.setImageResource(R.drawable.blue4);

        }
    }
}



