package com.example.ppmproject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Activity sending an http request to the users data server.
 * Fetch the data, then sort them.
 * Activity qui envoie une requete http au serveur contenant les informations des autres users.
 * On fetch les données, puis on les tri par catégorie(name, level etc...)
 */
public class UsersScreen extends AppCompatActivity {

    private List<TextView> textviews= new ArrayList();
    private  DownloadWebpageTask downloadTask = new DownloadWebpageTask();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users);

        textviews.add((TextView)findViewById(R.id.textView));
        textviews.add((TextView)findViewById(R.id.textView2));
        textviews.add((TextView)findViewById(R.id.textView3));
        textviews.add((TextView)findViewById(R.id.textView4));
        textviews.add((TextView)findViewById(R.id.textView5));

        for(TextView tv : textviews){
            tv.setText("");
        }

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Network available ...
             downloadTask.execute("https://www.lrde.epita.fr/~renault/teaching/ppm/2019/results.txt");

        } else {
            // Network not available ...
        }
    }


    private class DownloadWebpageTask
            extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call:
            // params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve URL (maybe invalid).";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        }

        private String downloadUrl(String myurl) throws IOException {
            InputStream is = null;
            // Only display the first 500 characters of the retrieved web page content.
            int len = 500;
            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();

                int response = conn.getResponseCode();
                is = conn.getInputStream();
                // Convert the InputStream into a string
                String contentAsString = readIt(is, len);
                String[] str = contentAsString.split("\n");
                StringBuffer buf = new StringBuffer();
                int j = 0;


                for(TextView tv : textviews) {
                    for (int i = 0; i < str.length && i < len; i++) {

                        String s = str[i].split(",")[j];
                        buf.append(s + "\n");

                    }
                    tv.setText(buf.toString());
                    buf.delete(0, buf.length());
                    j++;
                }

                return contentAsString;
                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            }
            catch (IOException e){
                e.printStackTrace();
            }
                finally {
                if (is != null)
                    is.close();
            }
            return "";
        }

        //transform the inpuStream into a String
        private String readIt(InputStream is, int len){
            StringBuffer buf = new StringBuffer();

            try {
                int c;
                while ((c = is.read()) != -1){
                        buf.append((char)c);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return buf.toString();
        }
    }


}
