package com.example.ppmproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Size;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;


public class DrawView extends View {
    private final int NB_SHIPS = 5;
    private Paint paint = null;
    private Path path = null;
    private List<Path> paths= new ArrayList<>();
    private List<Paint> paints= new ArrayList<>();

    public DrawView(Context context, AttributeSet attrs){
        super(context, attrs);

        for(int i = 0; i < NB_SHIPS; i++){
            paths.add(new Path());
        }
        for(int i = 0; i < NB_SHIPS; i++){
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(6f);
            paint.setColor(Color.RED);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paints.add(paint);

        }
        reset();
    }

    public void reset(){
        path = new Path();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(6f);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);

        invalidate();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        for(int i = 0; i < NB_SHIPS; i++) {
            if(path != null)
                canvas.drawPath(paths.get(i), paints.get(i));
        }
    }

    public void setPath(Path path, int id ){
        this.paths.get(id).reset();
        this.paths.get(id).addPath(path);
       invalidate();
    }
}




