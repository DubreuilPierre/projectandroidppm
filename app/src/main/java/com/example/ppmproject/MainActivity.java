package com.example.ppmproject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * Activity of the game.
 * C'est l'Activite du jeu en lui meme.
 */
public class MainActivity extends AppCompatActivity {

    private Timer timer = new Timer();
    private UserProfile userProfile;
    private List<Vector<PointFloat>> harbours;
    private List<ShipFragment> fragments = new ArrayList();
    private int nbParked = 0;
    private long firstTime;
    private final int NB_SHIPS = 5;
    private boolean once = true;
    private String title;
    private ImageView background;
    private ImageView explosion;
    private int cptExplosion = 0;
    private Timer timerExplosion = new Timer();
    private ImageView planet1;
    private ImageView planet2;
    private TextView loseView;
    private Button share;
    private Button playagain;
    private ProfileLine profileLineShare;
    private int cpt = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = getIntent().getExtras().getString("Title");
        userProfile = (UserProfile)getIntent().getExtras().getSerializable("Profile");

        TextView tv = findViewById(R.id.textView6);
        tv.setText(title);
        background = findViewById(R.id.backgroundView);
        background.setImageResource(R.drawable.backgroundspace);

        harbours = new ArrayList<>();
        explosion = findViewById(R.id.explosion);


        loseView = findViewById(R.id.loseTextView);
        share = findViewById(R.id.share);
        playagain = findViewById(R.id.playagain);


/**
 * On recupere tous nos vaisseaux, ils sont au nombre de 5
 */

        firstTime = System.currentTimeMillis();
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragments.add((ShipFragment)fragmentManager.findFragmentById(R.id.fragment));
        fragments.add((ShipFragment)fragmentManager.findFragmentById(R.id.fragment2));
        fragments.add((ShipFragment)fragmentManager.findFragmentById(R.id.fragment3));
        fragments.add((ShipFragment)fragmentManager.findFragmentById(R.id.fragment4));
        fragments.add((ShipFragment)fragmentManager.findFragmentById(R.id.fragment5));



        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



    }

    @Override
    protected void onStart() {
        if(once) {
            timer.scheduleAtFixedRate(new GameObserver(), 1000, 100);
            once = false;
        }

        super.onStart();
    }

    /**
     * Setup of all the on screen objects, define the planets coordinates, start with the spaceships out of the screen
     * Setup des objets sur la map, on définit les coordonnées des planetes, et l'on met les vaisseaux en dehors de l'ecran
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        background = findViewById(R.id.backgroundView);
        planet1 = findViewById(R.id.planet);
        planet2 = findViewById(R.id.planet1);

        planet1.setImageResource(R.drawable.planet2);
        planet2.setImageResource(R.drawable.planet6);

        planet2.setX(500.0f);
        planet2.setY(200.0f);

        planet1.setX(1200.0f);
        planet1.setY(400.0f);

        Vector harbour1 = new Vector();
        harbour1.add(0, new PointFloat(planet1.getX() + planet1.getWidth() / 2.0f, planet1.getY()) );
        harbour1.add(1, new PointFloat(planet1.getX() + planet1.getWidth(), planet1.getY() + planet1.getHeight()/2.0f) );
        harbour1.add(2, new PointFloat(planet1.getX() + planet1.getWidth() / 2.0f, planet1.getY() + planet1.getHeight()) );
        harbour1.add(3, new PointFloat(planet1.getX(), planet1.getY() + planet1.getHeight()/2.0f) );


        harbours.add(harbour1);


        Vector harbour2 = new Vector();
        harbour2.add(0, new PointFloat(planet2.getX() + planet2.getWidth() / 2.0f, planet2.getY()) );
        harbour2.add(1, new PointFloat(planet2.getX() + planet2.getWidth(), planet2.getY() + planet2.getHeight()/2.0f) );
        harbour2.add(2, new PointFloat(planet2.getX() + planet2.getWidth() / 2.0f, planet2.getY() + planet2.getHeight()) );
        harbour2.add(3, new PointFloat(planet2.getX(), planet2.getY() + planet2.getHeight()/2.0f) );
        harbours.add(harbour2);

        explosion.setImageResource(R.drawable.explosion0);

        for(ShipFragment f : fragments){
            f.setup(-f.getView().getWidth(), Integer.parseInt(f.getTag()) * f.getView().getHeight() + 100.0f * Integer.parseInt(f.getTag()), false);

        }

        super.onWindowFocusChanged(hasFocus);
    }

    /**
     * Lock here because we have to wait the timer to end before closing the app, otherwise crash.
     * Verrou ici car on doit attendre que le tick de timer se termine avant de quitter.
     */
    @Override
    protected void onStop() {
        timer.cancel();
        try {
            SaveToFile.saveTo(SaveToFile.USERPROFILE, getApplicationContext(), userProfile);
            SaveToFile.saveTo(SaveToFile.USERMOTTO, getApplicationContext(), userProfile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        synchronized (this) {

            super.onStop();
        }

    }

    /**
     * Save the profile info when leaving.
     * Si l'application est quitté on sauvegarde les informations du profil
     */
    @Override
    protected void onDestroy() {
        try {
            SaveToFile.saveTo(SaveToFile.USERPROFILE, getApplicationContext(), userProfile);
            SaveToFile.saveTo(SaveToFile.USERMOTTO, getApplicationContext(), userProfile);

        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


    /**
     * Game timer
     * C'est le timer qui observe le jeu.
     */
    class GameObserver extends TimerTask{
        @Override
        public void run() {
            int next = 0;
            int nbOnMap = 0;
            int nbCharged = 0;
            for(int i = 0; i < fragments.size();i++){
                if(!isOnMap(i)){
                    next = i;
                }
                else{
                    nbOnMap++;
                    if(fragments.get(i).isCharged()){
                        nbCharged++;
                    }
                }
            }

            /**
             * We launch a new spaceship only if there is less than 5 spaceship on the map.
             * Launch a new ship if none of them carries anything or with a propability of 20%
             *On ne lance un nouveau vaisseau que s'il y a moins de 5 vaisseaux sur la carte
             * On lance un vaisseau si aucun vaisseau n'a de cargaison ou avec une probabilite
             */
            if((nbCharged == 0 || Math.random() < 0.2) && (nbOnMap != NB_SHIPS )){
                float width = fragments.get(next).getView().getWidth();
                fragments.get(next).setup(- width, next * fragments.get(next).getView().getHeight()+ 100.0f * Integer.parseInt(fragments.get(next).getTag()),false);

            }

            synchronized (this) {
                for (int i = 0; i < fragments.size();i++) {
                    cpt = 0;
                    ShipFragment f = fragments.get(i);
                    if(f.getIsEntry()){
                        f.getView().setX(f.getView().getX() + 15.0f);
                    }

                    /**
                     * If two ships collide, end of the game.
                     * Les vaisseaux ne peuvent pas entrer en collisions lorsqu'ils sont en train de deposer une cargaison.
                     * Si deux vaisseaux se rencontrent, cela provoque une explosion et la fin du jeu
                     */
                    if(isOnMap(i)){
                        for(int j = 0 + cpt; j < fragments.size();j++){
                            if(fragments.get(i) != fragments.get(j)){
                                if(fragments.get(i).getEndAnimation() && fragments.get(j).getEndAnimation()) {
                                    if (fragments.get(i).isCollision(fragments.get(j)) && fragments.get(j).isCollision(fragments.get(i))) {
                                        timerExplosion = new Timer();
                                        timerExplosion.scheduleAtFixedRate(new Explosion(), 0, 500);
                                        explosion.setX(fragments.get(i).getExplosionPoint().getX() - explosion.getWidth() / 2.0f);
                                        explosion.setY(fragments.get(i).getExplosionPoint().getY() - explosion.getHeight() / 2.0f);
                                        for (ShipFragment f2 : fragments) {
                                            f2.end();
                                        }

                                        /**
                                         * When the game is done, record the score.
                                         * Enregistrement du score
                                         */
                                        long time = System.currentTimeMillis() - firstTime;
                                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                        String date = format.format(new Date());
                                        profileLineShare = new ProfileLine(userProfile.getName(), date, title, nbParked, (int) time/1000);
                                        userProfile.addUserProfile(userProfile.getName(), date, title, nbParked, (int) time/1000);
                                        try {
                                            SaveToFile.saveTo(SaveToFile.USERPROFILE, getApplicationContext(), userProfile);
                                            SaveToFile.saveTo(SaveToFile.USERMOTTO, getApplicationContext(), userProfile);

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        /**
                                         * Affichage du score
                                         */
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                loseView.setVisibility(View.VISIBLE);
                                                loseView.setText("You lose with "+nbParked+" parked ships");
                                                share.setVisibility(View.VISIBLE);
                                                playagain.setVisibility(View.VISIBLE);
                                            }
                                        });

                                        timer.cancel();
                                        return;
                                    }
                                }
                            }
                        }


                    }
                }

                /**
                 * Parking animation
                 * Trigger de l'animation de parking
                 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            for (int i = 0; i < fragments.size(); i++) {
                                int cpt = 0;

                                if (isOnMap(i)) {
                                    for (Vector<PointFloat> harbour : harbours) {
                                        PointFloat point = fragments.get(i).isParked(harbour.get(0), harbour.get(1), harbour.get(2), harbour.get(3));
                                        if (point.getX() != -1.0f) {
                                            if (cpt == 0) {
                                                fragments.get(i).park(point, planet1);
                                            } else {
                                                fragments.get(i).park(point, planet2);
                                            }
                                            nbParked++;
                                        }
                                        cpt++;
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }


    private boolean isOnMap(int ship){
        if((fragments.get(ship).getView().getX() < -fragments.get(ship).getView().getWidth() + 1) || (fragments.get(ship).getView().getX() > background.getWidth())){
            return false;
        }
        if(fragments.get(ship).getView().getY() > 2000.0)
            return false;

        return true;
    }

/**
 * End of the game animation
 * Animation de fn de jeu
 */
    private  class Explosion extends TimerTask {
        @Override
        public void run() {

            switch (cptExplosion) {

                case 0:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(ShipFragment f : fragments) {
                                f.end();
                            }
                        }
                    });
                    explosion.setImageResource(R.drawable.explosion0);
                    break;
                case 1:
                    explosion.setImageResource(R.drawable.explosion1);
                    break;
                case 2:
                    explosion.setImageResource(R.drawable.explosion2);
                    break;
                case 3:
                    explosion.setImageResource(R.drawable.explosion3);
                    break;
                default:
                    timerExplosion.cancel();
                    break;

            }
            cptExplosion++;
        }
    }

    /**
     * Reset le board
     * @param v
     */
    public void playAgain(View v){

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                for(ShipFragment f : fragments) {
                    f.setup(-f.getView().getWidth(), Integer.parseInt(f.getTag()) * f.getView().getHeight() + 100.0f * Integer.parseInt(f.getTag()), true);
                    f.getView().setVisibility(View.VISIBLE);
                }
                nbParked = 0;
                explosion.setVisibility(View.INVISIBLE);

            }
        });

        firstTime = System.currentTimeMillis();

        loseView.setVisibility(View.INVISIBLE);
        share.setVisibility(View.INVISIBLE);
        playagain.setVisibility(View.INVISIBLE);
        timer = new Timer();
        timer.scheduleAtFixedRate(new GameObserver(), 500, 100);

    }

    /**
     *Possibility to share the score.
     * Simple http post request
     */
    public void share(View v) throws IOException {
        v.setEnabled(false);
        DownloadWebpageTask downloadTask = new DownloadWebpageTask();

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Network available ...
            downloadTask.execute("http://192.168.1.23:3000/share");

        } else {
            // Network not available ...
            v.setEnabled(true);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loseView.setText("Connection error");
                }
            });

        }

    }

    /**
     * Si on clique sur share, envoie de la ligne de score au serveur
     */
    private class DownloadWebpageTask
            extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call:
            // params[0] is the url.
            try {
                System.out.println("TEST ");

                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve URL (maybe invalid).";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        }

        private String downloadUrl(String myurl) throws IOException {
            OutputStream os = null;
            // Only display the first 500 characters of the retrieved web page content.
            int len = 500;
            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("PUT");
                conn.setDoOutput(true);
                // Starts the query
                conn.connect();

                os = conn.getOutputStream();

                os.write(profileLineShare.toString().getBytes());

                int response = conn.getResponseCode();
                System.out.println("ResponseCode"+response);
                if(response == 200){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loseView.setText("Score shared");
                        }
                    });
                }
                return "";
                // Makes sure that the OutputStream is closed after the app is
                // finished using it.
            }
            catch (IOException e){
                e.printStackTrace();
            }
            finally {
                if (os != null)
                    os.close();
            }
            return "";
        }


    }

}
