package com.example.ppmproject;

import java.io.Serializable;

public class ProfileLine implements Serializable {
    private String name;
    private String date;
    private String levelName;
    private int numberParked;
    private int time;

    public String getName() {
        return name;
    }


    public ProfileLine(String name, String date, String levelName, int numberParked, int time){
        this.name = name;
        this.date = date;
        this.levelName = levelName;
        this.numberParked = numberParked;
        this.time = time;
    }

    public String toString(){
        return name+","+date+","+levelName+","+numberParked+','+time;
    }


}
