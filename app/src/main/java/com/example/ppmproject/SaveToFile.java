package com.example.ppmproject;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class  SaveToFile {

    public static final int USERPROFILE = 0;
    public static final int USERMOTTO= 1;
    public static final int USERNAME = 2;


    private static String path;

    public static void saveTo(int file, Context ctx, UserProfile userProfile) throws IOException {



        FileOutputStream fos = null;

        if(file == USERPROFILE) {
            path = "userProfile";

            fos = ctx.openFileOutput(path, Context.MODE_PRIVATE);


            for (ProfileLine profileLine : userProfile.getProfileLines()) {
                fos.write(profileLine.toString().getBytes());
                fos.write("\n".getBytes());

                System.out.println("PROFILELINE" + profileLine.toString());
            }



        }

        if(file == USERNAME) {
            path = "userName";

            fos = ctx.openFileOutput(path, Context.MODE_PRIVATE);

                fos.write(userProfile.getName().getBytes());
        }

        if(file == USERMOTTO){
            path = "userMotto";

            fos = ctx.openFileOutput(path, Context.MODE_PRIVATE);

                fos.write(userProfile.getMotto().getBytes());
        }

        fos.close();

        }

    public static UserProfile readFrom(int file, Context ctx ) {
        String ret = "";

        if(file == USERPROFILE){
            path = "userProfile";
        }

        try {
            InputStream input = ctx.openFileInput(path);
            if (input != null) {
                InputStreamReader inputSR = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputSR);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                    stringBuilder.append("\n");
                }
                input.close();
                System.out.println("READFROMPROFILE" + stringBuilder.toString());

                return new UserProfile(stringBuilder.toString());

            }
        }
        catch (Exception e){
                e.printStackTrace();
        }

        return new UserProfile();

    }

    public static String readFromMotto(int file, Context ctx ) {
        String ret = "";
        if (file == USERMOTTO) {
            path = "userMotto";
        }
        try {
            InputStream input = ctx.openFileInput(path);
            if (input != null) {
                InputStreamReader inputSR = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputSR);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                    stringBuilder.append("\n");

                }
                input.close();

                return stringBuilder.toString();

            }
        } catch (Exception e) {

        }
        return "";
    }

    public static String readFromUsername(int file, Context ctx ) {
        String ret = "";
        if (file == USERNAME) {
            path = "userName";
        }
        try {
            InputStream input = ctx.openFileInput(path);
            if (input != null) {
                InputStreamReader inputSR = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputSR);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                    stringBuilder.append("\n");

                }
                input.close();

                return stringBuilder.toString();

            }
        } catch (Exception e) {

        }
        return "";
    }




}
